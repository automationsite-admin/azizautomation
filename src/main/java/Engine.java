import com.sun.xml.internal.ws.api.client.WSPortInfo;
import school.students.Student;
import sun.invoke.empty.Empty;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Engine {
	public static String INVALID_USER_ENTERED = "please enter a country from the above";

	public static void main(String[] args) {
		ConnectingToSqlServer connectingToSqlServer = new ConnectingToSqlServer();
		connectingToSqlServer.connectingWithDatabase();

//		Engine engine = new Engine();

//		engine.checkingFileOperatingSystemPathForDirectory();

//		engine.deleteFile();
//		engine.writeToAFile();
//		engine.createFile();
//		engine.readAndWriteWithFileReader();
		//engine.writeToAFile();
		//engine.readAndWriteWithFileReader();
		//engine.writeToAFile();
//		engine.writeDataToAFile();
//		engine.createFile();
//		engine.tryingwhileLoop();
		//engine.methodToDeleteColorFromList();
		//engine.usingWhileLoop();
//		Student student = new Student();
//		student.setFirstName("Frederick");
//		System.out.println("First name: "+ student.getFirstName());
//		student.setLastName("Johnathan");
//		System.out.println("Last name: "+ student.getLastName());
//		usingMappToSortData();
//		usingMappingForStudent();
//		mixArryImplimentation();
//		verifyIntegerArray();
//		howTOUseArrayList();
//		printIntegerToConsole();
//		testStringArray();
//		decideUserNationality();

	}

	public static void printIntegerToConsole() {
		int[] age = {2, 2, 4, 5, 6};
		int x, i;
		for (i = 0; i < age.length; i++) {
			//x = age[i];
			System.out.println(age[i]);
		}
	}

	public static void testStringArray() {
		String[] family = new String[4];
		family[0] = "Father";
		family[1] = "Mother";
		family[2] = "Son";
		family[3] = "Daughter";
		for (String element : family) {
			System.out.println("values of family :" + element);
		}

		//System.out.println(family);
		/*String [] studentNames = {"nana","Tazoh","Lyce","Stacy","Aboh", "Sonia"};
		System.out.println(studentNames[4]);*/

	}

	/*
			write a computer program that ask a user for their Nationality and write back the
			following to the console on the user's response:
			[1] If a user inputs 'Uk', Write back "you are from Europe"
			[2] If a user inputs 'Nigeria' , Write back "You are from Africa"
			[3] If a user inputs 'Mexico' , Write back "You are from South America"
			[4] If a user didn't give a correct input, then write back "Your input is incorrect"
	*/
	public static String decideUserNationality() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter you nationality : ");
		String user = userInput.nextLine();
		user.equalsIgnoreCase("UK");
		switch (user) {
			case "UK":
				System.out.println("you are from Europe");
				break;
			case "Nigeria":
				System.out.println("You from Africa");
				break;
			case "Mexico":
				System.out.println("You are from South America");
			case "Asia":
				System.out.println("You are from China");
			default:
				System.out.println("Your input is incorrect");
		}
		return user;
	}
	public static void howTOUseArrayList(){
		List<String> studentName = new ArrayList<>(Arrays.asList("John","Jane","Bavo"));
		studentName.add("Sean");
		studentName.remove(3);
		studentName.remove("Jane");
		for(Object element: studentName){
			System.out.println(studentName);
		}
	}

	public static void verifyIntegerArray(int n){
		List<Integer> number = new ArrayList<>(Arrays.asList(2,4,56));
		for (Integer s : number){
				System.out.println(s);
			}
	}
	public static void mixArryImplimentation(){
		//ArrayList<java.io.Serializable> mixedStudentsAndValues = new ArrayList<java.io.Serializable>(Arrays.asList("nana","Sean",2,56,"Stacy"));
		ArrayList mixedStudentsAndValues = new ArrayList(Arrays.asList("nana","Sean",2,56,"Stacy"));
		mixedStudentsAndValues.add("Loyce");
		mixedStudentsAndValues.add(100);
		for(Object mix: mixedStudentsAndValues) {
			System.out.println(mix);
			System.out.println("===========");
		}
		mixedStudentsAndValues.remove("Sean");
		for (Object s: mixedStudentsAndValues) {
			System.out.println(s);
			System.out.println("++++++++++++++");
		}
		for (Object s: mixedStudentsAndValues){
			if(s instanceof String) {
				System.out.println(s +" =-=======");
			}
			if (s instanceof Integer) {
				System.out.println(s);
				System.out.println("-------- ");
			}
			if (s instanceof Boolean) {
				System.out.println(s+"--------------");

			}
		}

	}
	public static void usingMappingForStudent(){
		Map<String, Integer> studentsMarks = new HashMap<>();
		studentsMarks.put("James",35);
		studentsMarks.put("Susan",57);
		studentsMarks.put("Angela",59);
		studentsMarks.put("Brandon",78);

		studentsMarks.remove("Susan");
		for (int i = 0; i < studentsMarks.size();i++){
			System.out.println(studentsMarks.get(i).toString());
		}

		/*for (Map.Entry<String,Integer> marks : studentsMarks.entrySet()){
			System.out.println("Students Marks is : "+ marks);
			//System.out.println("Student name is : "+ marks.getKey() + "\t" +" Students Marks is :"+marks.getValue());
			//System.out.println("Student name is : "+ marks.getKey() + " | Students Marks is :"+marks.getValue());

		}
*/
	}
	public static void usingMappToSortData(){
		Map<String, Integer> studentsMarks = new HashMap<>();
		studentsMarks.put("James",10);
		studentsMarks.put("Susan",20);
		studentsMarks.put("Brandon",70);
		studentsMarks.put("Che",45);
		studentsMarks.put("Angela",40);
		studentsMarks.put("Neba",63);

		for (Map.Entry<String,Integer> marks : studentsMarks.entrySet()) {
			if (marks.getValue() >= 60 && marks.getValue() <= 100) {
				System.out.println("Student name is : "+ marks.getKey() + "\t" +" Students Marks is :"+marks.getValue());
				System.out.println("Student Grade is A");
			}
			if (marks.getValue() >= 40 && marks.getValue() <= 59) {
				System.out.println("Student name is : "+ marks.getKey() + "\t" +" Students Marks is :"+marks.getValue());
				System.out.println("Student Grade is B");
			}
			if (marks.getValue() >= 0 && marks.getValue() <= 39) {
				System.out.println("Student name is : "+ marks.getKey() + "\t" +" Students Marks is :"+marks.getValue());
				System.out.println("Student Grade is C");
			}
		}

	}
	public void usingWhileLoop(){
		int counter = 1;
		while (counter <=20){
			System.out.println("congratulations: "+ counter);
			counter ++;
		}
	}
	public void methodToDeleteColorFromList(){
		List<String > color = new ArrayList<>(Arrays.asList("Green", "Red","yello","blue","White"));
		color.add("");
		for (String c : color) {
			System.out.println("Array of Color: " + c);
		}
		if (color.contains("")){
		color.remove("");
		}
		color.remove("Red");
		for (String c: color)
			System.out.println("c is : "+ c);
		System.out.println();

	}
	public void tryingwhileLoop(){
		int counter = 1;
		int [] number = {1,2,3,5,6,8,9,23,45};
		while (counter <= number.length)
			System.out.println(Arrays.toString(number));
		counter++;
	}

	//Aziz File reader
	public void readContentFromFile(){
		StringBuilder reader = new StringBuilder();
		try {
			BufferedReader br = Files.newBufferedReader(Paths.get("C:\\Users\\FREDX\\Desktop\\GitBucket\\azizautomation\\src\\main\\resources\\TestData\\file.test.txt"));{
				String line;
				while ((line = br.readLine()) != null){
					reader.append(line).append("\n");
				}
			}
		}catch (IOException ex){
			System.err.format("IOException occurred , please check file format",ex);
		}
		System.out.println(reader);
	}

	public void writeDataToAFile(){
		try {
			List<String> list = new ArrayList();
			list.add("Line One");
			list.add("Line Two");
			list.add("ITUpskilling 2021");
			Files.write(Paths.get("C:\\Users\\FREDX\\Desktop\\GitBucket\\azizautomation\\src\\main\\resources\\TestData\\file.InsertData.txt"),list);
		}catch (IOException ex){
			System.err.format("IOException occurred , please check file format",ex);
		}
	}
	//using FileReader and FileInputStream to read  content of a file:
	//How to use FileWriter and FileOutputStream to write
	// 1st is to create a file object
	public void createFile(){
		File file = new File("C:\\Users\\FREDX\\Desktop\\GitBucket\\azizautomation\\src\\main\\resources\\TestData\\Java.txt");
		if (!file.exists()){
			try{
				file.createNewFile();
			}catch (IOException ex){
				ex.printStackTrace();
			}
		}
		System.out.println(file.getName());
		System.out.println(file.canWrite());
		System.out.println(file.getAbsoluteFile());
	}

	//File Writer and file Reader Method
	public void writeToAFile(){
		File files = new File("\"C:\\\\Users\\\\FREDX\\\\Desktop\\\\GitBucket\\\\azizautomation\\\\src\\\\main\\\\resources\\\\TestData\\\\Java.txt\"");
		if (!files.exists())
			try {
				FileWriter fileWriter = new FileWriter("Java.txt");
				fileWriter.write("Graduation day of Understanding\n"+"Father Christmas \n");
				fileWriter.write("Section One");
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
		}
		try {
			FileReader fr = new FileReader("Java.txt");
			int i;
			while((i = fr.read()) != -1){
				System.out.print((char)i);
			}
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// using file Input and file output stream to te read and Write to a file

	public void readAndWriteWithFileReader() {
		File file = new File("myFile.txt");
		try {
			FileOutputStream os = new FileOutputStream(file);
			String str = "Testing Os For file Reads";
			os.write(str.getBytes());
			os.flush();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try{
			FileInputStream fileInputStream = new FileInputStream("Java.txt");
			fileInputStream.read();
			fileInputStream.close();

		}catch (IOException ex){
			System.out.println(ex.fillInStackTrace());
		}
		file.delete();
	}

	//My test of deleting file
	public void deleteFile(){
		File filetoBeDeleted = new File("C:\\Users\\FREDX\\Desktop\\GitBucket\\azizautomation\\src\\main\\resources\\TestDatatestFile.txt");

		try {
			FileInputStream fileInputStream = new FileInputStream("TestDatatestFile.txt");
				if (!filetoBeDeleted.exists()){
					System.out.println("Please enter a valid file");
				}
				fileInputStream.close();
				} catch (IOException e){
			System.err.format("file not found",filetoBeDeleted);
			e.printStackTrace();
		}
		filetoBeDeleted.delete();
	}
	public void checkingFileOperatingSystemPathForDirectory(){
		//	Write this for mac to make our path operating system acnostic
//	String dirPath = "folder"+ File.separator + "anotherFolder";
//	Or
		//this will not run on mac but on windows
		String dirPath = "C:\\Users\\FREDX\\Desktop\\GitBucket\\azizautomation\\src\\main\\resources\\TestData\\";

		File dir = new File(dirPath);
		if (!dir.exists()){
			dir.mkdir();
		}
		//File file = new File(dirPath + File.separator + "testFile.txt");
		File file = new File(dirPath + "testFile.txt");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(file.getName());
		System.out.println(file.canWrite());
		System.out.println(file.getAbsoluteFile());


	}






}
