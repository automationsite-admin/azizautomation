public class Programming {
	public static void main(String [] args){
		System.out.println(" Hello World");
		//Q1: we will crete a method to compute simple interest. Simple Interest = Principal(AMOUNT OR MONEY) * Rate(PERCENTAGE) * Time(YEARS);
		//Q2: a client has reached out to you to write an application to calculate profit made on each item in their inventory. Write a method that
		// determines how much profit is made on an item given the cost and selling price
		//Q3: Write a method that will take someone's first name and last name and print out their full name

		double simpleInterest = calculateSimpleInterest(1000.00,24,0.8);
		printOutput(simpleInterest);

		double profit = calculateProfit(4, 2);
		printOutput(profit);

		String fullName = generateFullName("John","Walker-Sean");
		System.out.println(fullName);

		userName(24);
		costPerItem(100,80 );
	}

	public static double calculateSimpleInterest(double principal, double time, double rate ){
		double interest = (principal * time * rate )/ 100;
		return interest;
	}

	public static double calculateProfit(double productSellPrice, double productCost){
		double inventoryProfit = productSellPrice - productCost;
		return inventoryProfit;
	}

	public static String generateFullName(String firstName, String lastName){
		String fullName = firstName + " " + lastName;
		return fullName;
	}

	public static void printOutput(double input) {
		System.out.println(input);
	}
	public static int userName(int numb) {
		numb = 0;
		if (numb >= 0 && numb <= 17)
			System.out.println("You are not eligible for this service");
		if (numb >= 18 && numb < 80)
			System.out.println("You are eligible");
		if (numb >= 80 && numb <= 200)
			System.out.println("Please contact customer service");
		return numb;
	}
	public static double costPerItem(double itemSellingPrice, double itemCostPrice){
		double profitOfAnItem = itemSellingPrice - itemCostPrice;
		System.out.println("Profit made from this sale is "+ profitOfAnItem);
		return profitOfAnItem;
	}
}
