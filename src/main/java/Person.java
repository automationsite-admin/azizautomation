public class Person {
	private String firstName;
	private String lastName;
	private String discriminator;
	private String dateOfBirth;
	private int age;
	private String address;
	private  String phoneNumber;



	public String getFirstName(){
		return firstName;
	}
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public String getLastName(){
		return lastName;
	}public void setLastName(String lastName){
		this.lastName = lastName;
	}
	public String getDiscriminator(){
		return discriminator;
	}
	public void setDiscriminator(String discriminator){
		this.discriminator = discriminator;
	}
	public String getDateOfBirth(){
		return dateOfBirth;
	}
	public void setDateOfBirth(){
		this.dateOfBirth = dateOfBirth;
	}
	public int getAge(){
		return age;
	}
	public void setAge(int age){
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}




