public class CarPart {
	private String engine;
	private String  pedal;
	private String steering;
	private String tire;
	private String windScreen;
	private String sideMirror;
	private String wiper;

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getPedal() {
		return pedal;
	}

	public void setPedal(String pedal) {
		this.pedal = pedal;
	}

	public String getSteering() {
		return steering;
	}

	public void setSteering(String steering) {
		this.steering = steering;
	}

	public String getTire() {
		return tire;
	}

	public void setTire(String tire) {
		this.tire = tire;
	}

	public String getWindScreen() {
		return windScreen;
	}

	public void setWindScreen(String windScreen) {
		this.windScreen = windScreen;
	}

	public String getSideMirror() {
		return sideMirror;
	}

	public void setSideMirror(String sideMirror) {
		this.sideMirror = sideMirror;
	}

	public String getWiper() {
		return wiper;
	}

	public void setWiper(String wiper) {
		this.wiper = wiper;
	}
	public void carDrivingWheelsNeeded(){
		System.out.println("Tires needed");
	}
}
